<?php
use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;

return [
	'doctrine' => [
		'connection' => [
			'orm_default' => [
				'driverClass' => PDOMySqlDriver::class,
				'params' => [
					'host'     => '127.0.0.1',
					'user'     => 'krakosto_root',
					'password' => 'bartek',
					'dbname'   => 'krakosto_m1658',
				]
			],
		],
	],
];
