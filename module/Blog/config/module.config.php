<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Blog;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [
            'posts' => [
	            'type'    => Segment::class,
	            'options' => [
		            'route'    => '/posts[/:action[/:id]]',
		            'constraints' => [
			            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
			            'id' => '[0-9]*'
		            ],
		            'defaults' => [
			            'controller'    => Controller\PostController::class,
			            'action'        => 'index',
		            ],
	            ],
            ], 
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\PostController::class => Controller\Factory\PostControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
			Service\PostManager::class => Service\Factory\PostManagerFactory::class, 
        ],
    ],
	
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
	
	'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ]  
];
