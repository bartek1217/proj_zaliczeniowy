<?php
namespace Blog\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * This class represents a single post in a blog.
 * @ORM\Entity(repositoryClass="\Blog\Repository\PostRepository")
 * @ORM\Table(name="post")
 */
class Post
{
	const STATUS_DRAFT       = 1;
	const STATUS_PUBLISHED   = 2;
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	/**
	 * @ORM\Column(name="title")
	 */
	protected $title;
	/**
	 * @ORM\Column(name="content")
	 */
	protected $content;
	/**
	 * @ORM\Column(name="status")
	 */
	protected $status;
	/**
	 * @ORM\Column(name="date_created")
	 */
	protected $dateCreated;

	/**
	 * @ORM\OneToMany(targetEntity="\Blog\Entity\Comment", mappedBy="post")
	 * @ORM\JoinColumn(name="id", referencedColumnName="post_id")
	 */
	protected $comments;


	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->comments = new ArrayCollection();
	}
	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * Sets ID of this post.
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	/**
	 * @return integer
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @param integer $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param type $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	/**
	 * @param string $dateCreated
	 */
	public function setDateCreated($dateCreated)
	{
		$this->dateCreated = $dateCreated;
	}

	/**
	 * @return array
	 */
	public function getComments()
	{
		return $this->comments;
	}

	/**
	 * @param $comment
	 */
	public function addComment($comment)
	{
		$this->comments[] = $comment;
	}
}