<?php
namespace Blog\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Blog\Form\PostForm;
use Blog\Entity\Post;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Blog\Form\CommentForm;

class PostController extends AbstractActionController
{
	/**
	 * Entity manager.
	 * @var Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	/**
	 * Post manager.
	 * @var Application\Service\PostManager
	 */
	private $postManager;


	public function __construct($entityManager, $postManager)
	{
		$this->entityManager = $entityManager;
		$this->postManager = $postManager;
	}


	public function addAction()
	{
		$form = new PostForm();

		if ($this->getRequest()->isPost()) {

			$data = $this->params()->fromPost();

			$form->setData($data);
			if ($form->isValid()) {

				$data = $form->getData();

				$this->postManager->addNewPost($data);

				return $this->redirect()->toRoute('posts');
			}
		}

		return new ViewModel([
			'form' => $form
		]);
	}

	public function editAction()
	{
		$form = new PostForm();

		$postId = $this->params()->fromRoute('id', -1);

		$post = $this->entityManager->getRepository(Post::class)
		                            ->findOneById($postId);
		if ($post == null) {
			$this->getResponse()->setStatusCode(404);
			return;
		}

		if ($this->getRequest()->isPost()) {

			$data = $this->params()->fromPost();

			$form->setData($data);
			if ($form->isValid()) {

				$data = $form->getData();

				$this->postManager->updatePost($post, $data);

				return $this->redirect()->toRoute('posts', ['action'=>'admin']);
			}
		} else {
			$data = [
				'title' => $post->getTitle(),
				'content' => $post->getContent(),
				'status' => $post->getStatus()
			];

			$form->setData($data);
		}

		return new ViewModel([
			'form' => $form,
			'post' => $post
		]);
	}

	public function deleteAction()
	{
		$postId = $this->params()->fromRoute('id', -1);

		$post = $this->entityManager->getRepository(Post::class)
		                            ->findOneById($postId);
		if ($post == null) {
			$this->getResponse()->setStatusCode(404);
			return;
		}

		$this->postManager->removePost($post);

		return $this->redirect()->toRoute('posts', ['action'=>'admin']);
	}


	public function viewAction()
	{
		$postId = $this->params()->fromRoute('id', -1);

		$post = $this->entityManager->getRepository(Post::class)
		                            ->findOneById($postId);

		if ($post == null) {
			$this->getResponse()->setStatusCode(404);
			return;
		}

		$commentCount = $this->postManager->getCommentCountStr($post);

		$form = new CommentForm();

		if($this->getRequest()->isPost()) {

			$data = $this->params()->fromPost();

			$form->setData($data);
			if($form->isValid()) {

				$data = $form->getData();

				$this->postManager->addCommentToPost($post, $data);

				return $this->redirect()->toRoute('posts', ['action'=>'view', 'id'=>$postId]);
			}
		}

		return new ViewModel([
			'post' => $post,
			'commentCount' => $commentCount,
			'form' => $form,
			'postManager' => $this->postManager
		]);
	}
	
	public function indexAction()
	{
		$page = $this->params()->fromQuery('page', 1);
		$query = $this->entityManager->getRepository(Post::class)->findPublishedPosts();

		$adapter = new DoctrineAdapter(new ORMPaginator($query, false));
		$paginator = new Paginator($adapter);
		$paginator->setDefaultItemCountPerPage(4);
		$paginator->setCurrentPageNumber($page);

		return new ViewModel([
			'posts' => $paginator,
			'postManager' => $this->postManager
		]);
	}
	
	public function adminAction()
	{
		$posts = $this->entityManager->getRepository(Post::class)
		                             ->findBy([], ['dateCreated'=>'DESC']);

		return new ViewModel([
			'posts' => $posts,
			'postManager' => $this->postManager
		]);
	}


}
