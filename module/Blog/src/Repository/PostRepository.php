<?php
namespace Blog\Repository;

use Doctrine\ORM\EntityRepository;
use Blog\Entity\Post;

class PostRepository extends EntityRepository
{

	public function findPublishedPosts()
	{
		$entityManager = $this->getEntityManager();

		$queryBuilder = $entityManager->createQueryBuilder();

		$queryBuilder->select('p')
		             ->from(Post::class, 'p')
		             ->where('p.status = ?1')
		             ->orderBy('p.dateCreated', 'DESC')
		             ->setParameter('1', Post::STATUS_PUBLISHED);

		return $queryBuilder->getQuery();
	}

}
