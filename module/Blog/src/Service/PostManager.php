<?php
namespace Blog\Service;

use Blog\Entity\Post;
use Blog\Entity\Comment;
use Zend\Filter\StaticFilter;

class PostManager
{
	/**
	 * Doctrine entity manager.
	 * @var Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	public function __construct($entityManager)
	{
		$this->entityManager = $entityManager;
	}

	public function addNewPost($data)
	{
		$post = new Post();
		$post->setTitle($data['title']);
		$post->setContent($data['content']);
		$post->setStatus($data['status']);
		$currentDate = date('Y-m-d H:i:s');
		$post->setDateCreated($currentDate);

		$this->entityManager->persist($post);

		$this->entityManager->flush();
	}

	
	public function updatePost($post, $data)
	{
		$post->setTitle($data['title']);
		$post->setContent($data['content']);
		$post->setStatus($data['status']);

		$this->entityManager->flush();
	}

	public function removePost($post)
	{
		$comments = $post->getComments();
		foreach ($comments as $comment) {
			$this->entityManager->remove($comment);
		}

		$this->entityManager->remove($post);

		$this->entityManager->flush();
	}

	public function getCommentCountStr($post)
	{
		$commentCount = count($post->getComments());
		if ($commentCount == 0)
			return 'Brak komentarzy';
		else if ($commentCount == 1)
			return '1 komentarz';
		else
			return $commentCount . ' komentarzy';
	}


	public function addCommentToPost($post, $data)
	{
		$comment = new Comment();
		$comment->setPost($post);
		$comment->setAuthor($data['author']);
		$comment->setContent($data['comment']);
		$currentDate = date('Y-m-d H:i:s');
		$comment->setDateCreated($currentDate);

		$this->entityManager->persist($comment);

		$this->entityManager->flush();
	}

	public function getPostStatusAsString($post)
	{
		switch ($post->getStatus()) {
			case Post::STATUS_DRAFT: return 'Szkic';
			case Post::STATUS_PUBLISHED: return 'Opublikowany';
		}

		return 'Unknown';
	}
}