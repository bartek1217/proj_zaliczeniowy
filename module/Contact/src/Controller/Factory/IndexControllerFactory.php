<?php
namespace Contact\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Contact\Controller\IndexController;
use Contact\Service\MailSender;

/**
 * This is the factory for IndexController. Its purpose is to instantiate the
 * controller.
 */
class IndexControllerFactory implements FactoryInterface
{
	public function __invoke(ContainerInterface $container,
		$requestedName, array $options = null)
	{
		$mailSender = $container->get(MailSender::class);

		// Instantiate the controller and inject dependencies
		return new IndexController($mailSender);
	}
}
