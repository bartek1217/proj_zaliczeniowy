<?php
namespace Contact\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Contact\Service\MailSender;
use Contact\Form\ContactForm;

class IndexController extends AbstractActionController
{
	/**
	 * Post manager.
	 * @var Contact\Service\PostManager
	 */
	private $mailSender;

	public function __construct($mailSender)
	{
		$this->mailSender = $mailSender;
	}

	public function contactUsAction()
	{
		// Create Contact Us form
		$form = new ContactForm();

		// Check if user has submitted the form
		if($this->getRequest()->isPost()) {

			// Fill in the form with POST data
			$data = $this->params()->fromPost();

			$form->setData($data);

			// Validate form
			if($form->isValid()) {

				// Get filtered and validated data
				$data = $form->getData();
				$email = $data['email'];
				$subject = $data['subject'];
				$body = $data['body'];

				// Send E-mail
				if(!$this->mailSender->sendMail($email, 'mposa92@gmail.com',
					$subject, $body)) {
					// In case of error, redirect to "Error Sending Email" page
					return $this->redirect()->toRoute('contactus',
						['action'=>'sendError']);
				}

				// Redirect to "Thank You" page
				return $this->redirect()->toRoute('contactus',
					['action'=>'thankYou']);
			}
		}

		// Pass form variable to view
		return new ViewModel([
			'form' => $form
		]);
	}

	// This action displays the Thank You page. The user is redirected to this
	// page on successful mail delivery.
	public function thankYouAction()
	{
		return new ViewModel();
	}

	// This action displays the Send Error page. The user is redirected to this
	// page on mail delivery error.
	public function sendErrorAction()
	{
		return new ViewModel();
	}
}