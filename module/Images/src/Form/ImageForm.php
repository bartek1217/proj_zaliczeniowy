<?php
namespace Images\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

// uploadowanie zdjecia
class ImageForm extends Form
{

	// konstruktor
	public function __construct()
	{
		// definicja nazwy formularza
		parent::__construct('image-form');

		// ustawienie metody 'post' dla tego formularza
		$this->setAttribute('method', 'post');

		// ustawienie kodowanie binarnego
		$this->setAttribute('enctype', 'multipart/form-data');

		$this->addElements();

		$this->addInputFilter();
	}

	// funkcja dodajaca elementy do formularza
	protected function addElements()
	{
		// pole plik
		$this->add([
			'type'  => 'file',
			'name' => 'file',
			'attributes' => [
				'id' => 'file'
			],
			'options' => [
				'label' => 'Rozmiar pliku',
			],
		]);

		// przycisk submit
		$this->add([
			'type'  => 'submit',
			'name' => 'submit',
			'attributes' => [
				'value' => 'Wyślij',
				'id' => 'submitbutton',
			],
		]);
	}

	// funkcja tworzaca filtrowanie inputa (walidacja)
	private function addInputFilter()
	{
		$inputFilter = new InputFilter();
		$this->setInputFilter($inputFilter);

		// reguly walidacji dla pola plik
		$inputFilter->add([
			'type'     => 'Zend\InputFilter\FileInput',
			'name'     => 'file',
			'required' => true,
			'validators' => [
				['name'    => 'FileUploadFile'],
				[
					'name'    => 'FileMimeType',
					'options' => [
						'mimeType'  => ['image/jpeg', 'image/png']
					]
				],
				['name'    => 'FileIsImage'],
				[
					'name'    => 'FileImageSize',
					'options' => [
						'minWidth'  => 128,
						'minHeight' => 128,
						'maxWidth'  => 4096,
						'maxHeight' => 4096
					]
				],
			],
			'filters'  => [
				[
					'name' => 'FileRenameUpload',
					'options' => [
						'target'=>'./data/upload',
						'useUploadName'=>true,
						'useUploadExtension'=>true,
						'overwrite'=>true,
						'randomize'=>false
					]
				]
			],
		]);
	}
}