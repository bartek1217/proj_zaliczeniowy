<?php
namespace Images\Service;

class ImageManager
{
	// sciezka gdzie beda zapisywane zdjecia
	private $saveToDir = './data/upload/';

	// zwraca sciezke do folderu gdzie zapisujemy zdjecia
	public function getSaveToDir()
	{
		return $this->saveToDir;
	}

	// zwraca tablice z nazwami plikow zdjec
	public function getSavedFiles()
	{

		// sprawdza czy folder istnieje, jesli nie to go tworzy
		if(!is_dir($this->saveToDir)) {
			if(!mkdir($this->saveToDir)) {
				throw new \Exception('Nie można utworzyć folderu ' .
				                     error_get_last());
			}
		}

		// sprawdza folder i tworzy liste uploadowanych plikow
		$files = [];
		$handle  = opendir($this->saveToDir);
		while (false !== ($entry = readdir($handle))) {

			if($entry=='.' || $entry=='..')
				continue; // pomija aktualny i nadrzędny folder

			$files[] = $entry;
		}

		// zwraca liste uploadowanych plikow
		return $files;
	}

	// zwraca sciezke do zapisnych plikow zdjec
	public function getImagePathByName($fileName)
	{
		// zabezieczenie nazwy pliku usuwanie slashy
		$fileName = str_replace("/", "", $fileName);
		$fileName = str_replace("\\", "", $fileName);

		// zwrocenie nazwy folderu i zmienionej nazwy pliku
		return $this->saveToDir . $fileName;
	}

	// zwraca zawartosc pliku zdjecia
	public function getImageFileContent($filePath)
	{
		return file_get_contents($filePath);
	}

	// pobiera informacje o plikach (rozmiar, typ MIME) według ścieżki obrazu.
	public function getImageFileInfo($filePath)
	{
		// otwarcie pliku
		if (!is_readable($filePath)) {
			return false;
		}

		// pobranie rozmiaru pliku
		$fileSize = filesize($filePath);

		// pobranie typu MIME pliku
		$finfo = finfo_open(FILEINFO_MIME);
		$mimeType = finfo_file($finfo, $filePath);
		if($mimeType===false)
			$mimeType = 'application/octet-stream';

		return [
			'size' => $fileSize,
			'type' => $mimeType
		];
	}

	// zmiana wymiarow zachowujac aspekt zdjec
	public  function resizeImage($filePath, $desiredWidth = 240)
	{
		// pobranie oryginalnego rozmiaru
		list($originalWidth, $originalHeight) = getimagesize($filePath);

		// obliczenie aspektu
		$aspectRatio = $originalWidth/$originalHeight;

		// obliczenie pozadanej wysokosci
		$desiredHeight = $desiredWidth/$aspectRatio;

		// pobranie informacji o zdjeciu
		$fileInfo = $this->getImageFileInfo($filePath);

		// zmiana wymiarow
		$resultingImage = imagecreatetruecolor($desiredWidth, $desiredHeight);
		if (substr($fileInfo['type'], 0, 9) =='image/png')
			$originalImage = imagecreatefrompng($filePath);
		else
			$originalImage = imagecreatefromjpeg($filePath);
		imagecopyresampled($resultingImage, $originalImage, 0, 0, 0, 0,
			$desiredWidth, $desiredHeight, $originalWidth, $originalHeight);

		// zapisanie przeskalowanego zdjecia do tymczasowego folderu
		$tmpFileName = tempnam("/tmp", "FOO");
		imagejpeg($resultingImage, $tmpFileName, 80);

		// zwrocenie sciezki do wynikowego zdjecia
		return $tmpFileName;
	}
}