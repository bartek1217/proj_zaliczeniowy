<?php
namespace Images\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Images\Form\ImageForm;

class ImageController extends AbstractActionController
{
	private $imageManager;

	public function __construct($imageManager)
	{
		$this->imageManager = $imageManager;
	}


	public function indexAction()
	{
		// pobranie listy aktualnie zapisanych plikow
		$files = $this->imageManager->getSavedFiles();

		// renderowanie strony
		return new ViewModel([
			'files'=>$files
		]);
	}


	public function uploadAction()
	{
		// tworzenie nowego modelu formularza
		$form = new ImageForm();

		// sprawdzenie czy uzytkownik wyslal formularz
		if($this->getRequest()->isPost()) {

			// upewnienie się o scaleniu informacji o plikach
			$request = $this->getRequest();
			$data = array_merge_recursive(
				$request->getPost()->toArray(),
				$request->getFiles()->toArray()
			);

			// przekazanie danych do formularza
			$form->setData($data);

			// walidacja formularza
			if($form->isValid()) {

				// przeniesienie wyslanych plikow do folderu
				$data = $form->getData();

				// przekierowanie uzytkownika do galerii zdjec
				return $this->redirect()->toRoute('images');
			}
		}

		// renderowanie strony
		return new ViewModel([
			'form' => $form
		]);
	}

	public function fileAction()
	{
		// pobranie nazwy pliku
		$fileName = $this->params()->fromQuery('name', '');

		// sprawdzenie czy uzytkownik potrzebuje miniaturki czy pelnego rozmiaru zdjecia
		$isThumbnail = (bool)$this->params()->fromQuery('thumbnail', false);

		// pobranie sciezki do pliku
		$fileName = $this->imageManager->getImagePathByName($fileName);

		if($isThumbnail) {

			// przeskalowanie zdjecia
			$fileName = $this->imageManager->resizeImage($fileName);
		}

		// pobranie informacji o zdjeciu (rozmiar i typ MIME)
		$fileInfo = $this->imageManager->getImageFileInfo($fileName);
		if ($fileInfo===false) {
			$this->getResponse()->setStatusCode(404);
			return;
		}

		// zapis naglowkow HTtP
		$response = $this->getResponse();
		$headers = $response->getHeaders();
		$headers->addHeaderLine("Content-type: " . $fileInfo['type']);
		$headers->addHeaderLine("Content-length: " . $fileInfo['size']);

		// zapis zawartosci pliku
		$fileContent = $this->imageManager->getImageFileContent($fileName);
		if($fileContent!==false) {
			$response->setContent($fileContent);
		} else {
			// ustawienie statusu bledu na kod 500
			$this->getResponse()->setStatusCode(500);
			return;
		}

		if($isThumbnail) {
			// usuniecie tymczasowej miniaturki pliku
			unlink($fileName);
		}

		// unikniecie domyslnego widoku
		return $this->getResponse();
	}
}
