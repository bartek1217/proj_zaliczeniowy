<?php
namespace Images\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Images\Service\ImageManager;
use Images\Controller\ImageController;


class ImageControllerFactory implements FactoryInterface
{
	public function __invoke(ContainerInterface $container,
		$requestedName, array $options = null)
	{
		$imageManager = $container->get(ImageManager::class);

		return new ImageController($imageManager);
	}
}
