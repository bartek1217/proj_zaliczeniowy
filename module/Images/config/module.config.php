<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Images;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'images' => [
	            'type'    => Segment::class,
	            'options' => [
		            'route'    => '/images[/:action]',
		            'constraints' => [
			            'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
		            ],
		            'defaults' => [
			            'controller'    => Controller\ImageController::class,
			            'action'        => 'index',
		            ],
	            ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\ImageController::class => Controller\Factory\ImageControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\ImageManager::class => InvokableFactory::class,  
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ], 
];
