Projekt:

Tytul:	Strona pasjonatki fotografii przyrodniczej

Specyfikacja:

Strona po�wi�cona fotografii. Na danej stronie mo�na przeczyta� kr�tki wst�p o mnie.

Na stronie znajduj� si� aktualno�ci, galeria zdj�� oraz formularz kontaktowy.

Modu�y:

-> strona g��wna (statyczny modu� z kr�tkim opisem "O mnie")

-> aktualno�ci w formie bloga z mo�liwo�ci� komentowania wpis�w (+ panel admina)

-> galeria zdj�� z mo�liwo�ci� dodawania zdj��

-> formularz kontaktowy

Przygotowanie:

Do projektu do��czony zosta� plik m1658_blog.sql z baz� danych, nale�y go pobra�.

Kolejne kroki do wykonania:

1. Zmiana portu mysql na 3306

2. Utworzenie bazy danych m1658_blog, import bazy z pliku(m1658_blog.sql)

3. Zmiana ustawie� w php.ini

	-> Odkomentowanie ;date.timezone = Europe/Paris oraz ;extension=php_fileinfo.dll
	
4. Wykonanie polecenia composer update w folderze projektu

Kolejne kroki do wykonania (USBWebserver):

1. Uruchomi� USBWebserver i zmieni� port mysql na 3306

2. Zalogowa� si� do phpmyadmin i utworzy� baz� danych o nazwie m1658_blog

3. Wej�� w utworzon� baz� danych i zaimportowa� do niej plik z baz� m1658_blog.sql

4. Wej�� do folderu /USBWebserver/settings/ i edytowa� plik php.ini

	-> Odkomentowa� lini� 912 ;date.timezone = Europe/Paris (usun�� �rednik na pocz�tku linii) 	
	
		#ustwienie czasu
		
	-> Odkomentowa� lini� 863 ;extension=php_fileinfo.dll 
	
		#�adowanie zdj��
		
5. Wykonanie polecenia composer update w folderze projektu	

6. Uruchomi� ponownie USBWebserver
