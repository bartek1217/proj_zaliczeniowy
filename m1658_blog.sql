-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 01 Cze 2017, 17:12
-- Wersja serwera: 5.7.16-10-log
-- Wersja PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `m1658_blog`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(128) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='utf8_general_ci';

--
-- Zrzut danych tabeli `comment`
--

INSERT INTO `comment` (`id`, `post_id`, `content`, `author`, `date_created`) VALUES
(5, 5, 'Piękny opis. Pozdrawiam', 'admin', '2017-05-31 23:17:49'),
(6, 8, 'Niesamowite!', 'admin', '2017-05-31 23:42:44');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='utf8_general_ci';

--
-- Zrzut danych tabeli `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `status`, `date_created`) VALUES
(2, 'Budowa obiektywÃ³w', 'Lorem ipsum dolor sit amet felis. Vestibulum enim. Mauris imperdiet, lorem eros porttitor egestas. Cum sociis natoque penatibus et imperdiet quis, porttitor laoreet ut, nonummy nisl felis fermentum nonummy at, quam. Maecenas mi porttitor dui. Cras egestas dignissim, lorem sapien, lacinia eget, accumsan ullamcorper in, augue. Sed nec diam. Fusce nulla. Aenean ut nunc sem, eleifend tincidunt, velit vel quam. Curabitur enim. Phasellus consequat. Integer adipiscing. Mauris molestie justo nec tellus. Curabitur ornare ac, laoreet ante pulvinar ut, nonummy sagittis. Lorem ipsum ultricies in, odio. Nam libero nec nunc hendrerit id, elit. Mauris ultrices. Sed tincidunt lorem. Cras luctus ut, diam. Donec fermentum eget, vestibulum quis, placerat porttitor. Maecenas at nulla. Mauris mattis eget, facilisis luctus eget, molestie turpis rutrum id, urna. Nulla mi quam, ultrices varius. Ut tincidunt et.', 2, '2017-05-31 11:21:54'),
(3, 'Specjalne rodzaje obiektywÃ³w fotograficznych', 'Lorem ipsum dolor sit amet tellus. Nulla semper. Sed adipiscing gravida elit. Aliquam sed eros ipsum, vel neque odio lobortis laoreet ligula quis justo. Integer a venenatis risus. Vivamus euismod. Vestibulum vulputate. Morbi urna ullamcorper lorem. Vestibulum et mauris sit amet ligula. Lorem ipsum primis in augue. Donec mauris non nunc. Vestibulum ante sem at augue nec sem ut tellus dolor ut tortor metus bibendum eget, enim. Cras vitae eros vulputate molestie. Quisque ornare dolor urna dapibus vitae, cursus ligula felis, feugiat felis. Curabitur condimentum tempor, dolor sit amet, consectetuer eget, enim. Suspendisse eleifend posuere sed, ornare nisl, blandit iaculis, diam magna ultrices posuere eu, luctus nec, lacinia dignissim, libero quis suscipit faucibus scelerisque. Duis porttitor magna. Vestibulum ante sit amet quam. Curabitur est et justo. Aliquam fringilla, justo nulla.', 2, '2017-05-31 12:32:30'),
(5, 'Wady optyczne obiektywu', 'Lorem ipsum dolor sit amet nibh. Morbi egestas, dapibus nisl tristique congue. Lorem ipsum aliquet lacinia quis, accumsan augue ut quam interdum elementum. Mauris et ultrices tincidunt, risus commodo ligula non sem. Suspendisse in sem. Quisque cursus a, dolor. Ut sodales pretium at, ligula. Sed nibh. Ut sit amet enim. Duis consequat eu, posuere cubilia Curae, Duis ante id diam at tellus. Morbi tellus enim, aliquet elit ac augue. Maecenas mi augue, id mauris sed libero quis wisi. Donec erat fermentum a, dolor. Nullam sapien. Cras vitae sem. In laoreet vulputate at, fermentum tortor. Aliquam sit amet sapien leo sed elit laoreet urna eu arcu elit, sit amet metus. Aliquam ante ipsum primis in interdum at, aliquet at, malesuada tristique, mauris nec tellus. Vestibulum ut tortor. Donec molestie sagittis. Curabitur.', 2, '2017-05-31 19:21:15'),
(8, 'Podstawowe cechy i parametry obiektywu', 'Lorem ipsum dolor sit amet magna. Integer vestibulum faucibus orci consequat hendrerit. Fusce venenatis nulla ac nibh ultricies ut, faucibus orci diam, suscipit wisi. Proin nonummy sagittis. Nulla semper. Morbi id rutrum ac, rhoncus placerat ante. Maecenas tortor vehicula ullamcorper. Nam rhoncus, dolor leo lacus, elementum congue, lacus vehicula neque. Donec auctor congue ac, felis. Pellentesque sed eros vulputate aliquam iaculis lectus. Phasellus hendrerit id, ultrices posuere lobortis, massa vulputate aliquam dictum arcu. Vestibulum ut quam. Aliquam id nulla ac lacus. Maecenas quam quis nibh. Curabitur et magnis dis parturient montes, nascetur ridiculus mus. Integer sit amet nibh. Phasellus in faucibus orci ut diam ut sapien. Aliquam sem. Nulla hendrerit magna dolor, luctus nulla ornare euismod. Vestibulum non sem ullamcorper eleifend ac, pede. Donec suscipit congue non, tristique ullamcorper. ', 2, '2017-05-31 23:42:32'),
(9, 'Fotografia tradycyjna a analogowa', 'Lorem ipsum dolor sit amet dignissim volutpat ut, consequat id, imperdiet neque. Suspendisse ac ornare ornare. Nullam in wisi. Praesent in vestibulum iaculis. Sed gravida eros magna dolor, placerat eget, bibendum ipsum ut pede. Morbi consequat urna eget dolor. Donec tortor mauris, interdum libero. Curabitur nunc. Maecenas quis venenatis interdum, tortor metus nunc, sed porta neque. Cras hendrerit wisi. In mauris sit amet, consectetuer adipiscing elit. Lorem ipsum primis in ligula quis massa volutpat a, iaculis odio, in consequat lacus iaculis malesuada tristique, augue quis neque. Fusce sed neque. Praesent nec tellus. Cras interdum adipiscing metus. Morbi mauris non diam. Phasellus id felis. Quisque tortor. Nulla nec tellus. Morbi molestie, nunc venenatis in, ante. Duis sit amet tellus non purus. Phasellus at nibh. Morbi dignissim, tellus. Donec at eros ultrices.', 2, '2017-05-31 23:46:55');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
